import socket
import threading
import socketserver
import sys
import json
import urllib.request
import time
import sqlite3

#These are the containers for the add queue and database contents
#The add queue is used as a thread-safe way to add onto the database
#When an element needs to be added, it will be given to the queue, which
#Will then only add it inside the loop in the main function, keeping it thread safe.
array = []

#These arrays will be populated and used as a check against getting
#information we already have (because our API calls are limited)
validnames = []
validdict = {}

#This section loads in the SQL database or creates a new one if one does not exist.
conn = sqlite3.connect('ranked.db')
curs = conn.cursor()
curs.execute('create table if not exists RANKED ( id text primary key, info text )')
conn.commit()


for row in curs.execute('SELECT * FROM RANKED'):
    validnames.append(row[0]) #add name to the validnames
    validdict[row[0]] = json.loads(row[1])

#This is used as a reference point for API calls. The problem with this program
#As it is now is that without an upgraded API key, Riot Games only allows 500
#API calls every ten minutes. An upgraded API key can take 180,000 API calls per 10 minutes
#But you need to show a finished, refined product to Riot Games to be considered for one.
lasttime = 0



#This is the thread called when a connection is received. A new one is created for each
#Connection the server receives.
class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):
    
    def handle(self):
        global array, lasttime, validnames, validdict

        TIMEOUT = 24 * 60 * 60 #This is the number of seconds in a day and how often an
                                #Entry can be refreshed with consideration to API calls
        apikey = 'f3efce89-9fa0-45f2-be79-14287353f42b'
    
        url1 = 'https://na.api.pvp.net'

        #Summoner Name to get Summoner ID
        url2 = '/api/lol/na/v1.4/summoner/by-name/'
        urlend = '?api_key='

        #Ranked Summary summonerID in
        url4 = '/api/lol/na/v1.3/stats/by-summoner/'
        url5 = '/ranked'

        #Last ten matches SummonerID in
        url6 = '/api/lol/na/v2.2/matchlist/by-summoner/'
        url7 = '?rankedQueues=RANKED_SOLO_5x5&beginIndex=0&endIndex=10&api_key='

        #Match MatchID in
        url8 = '/api/lol/na/v2.2/match/'
        hdr = { 'User-Agent' : 'Developer VoloDomini' }

        #The return dictionary (basically a json element) will be stored here
        datadict = {}

        data = str(self.request.recv(1024), 'ascii')
        search = ''

        #We're checking the updated dictionary (not database, but created from the database
        #and continually updated) for the name given. If its there, add to the return and
        #don't search it.
        for spl in data.split(","):
            if spl in validnames and time.time() - validdict[spl]['LstUpd'] < (TIMEOUT):
                datadict[spl] = validdict[spl]
            else:
                search = search + spl + ","
        search = search.rstrip(",")
                
        #If the search string generated above is not empty, API call.
        if search != '':
            
            FURL = url1 + url2 + search + urlend + apikey
            FURL = FURL.replace(" ", '%20')

            read = False
            invName = False

            
            while not read:
                if time.time() - lasttime > 1.3:
                    lasttime = time.time()
                    try:
                        #This is the API call, it returns elements in a json format
                        req = urllib.request.Request(FURL, headers=hdr)
                        with urllib.request.urlopen(req) as response:
                                the_page = json.loads(response.read().decode("UTF-8"))
                                
                        read = True
                        
                    except urllib.error.HTTPError as err:
                        if str(err).rstrip() != "HTTP Error 429: Too Many Requests":
                                invName = True
                                print(err)
                                break
                        
                        time.sleep(1.5)
            #An invalid name will return a 404. It is important to check for that here
            #Otherwise there will be an error
            if not invName:
                #Get name and summonerID and store to a dictionary
                for ele in the_page:
                    datadict[the_page[ele]['name'].lower()] = {}
                    datadict[the_page[ele]['name'].lower()]['id'] = the_page[ele]['id']
                    datadict[the_page[ele]['name'].lower()]['stats'] = {}

                #At this point, we have the summonerID and name in a dictionary
                #Let's get a ranked summary
                for dat in datadict: #names
                    SumID = datadict[dat]['id']

                    FURL = url1 + url4 + str(SumID) + url5 + urlend + apikey
                    read = False
                    invName = False
                    while not read:
                        if time.time() - lasttime > 1.3:
                            lasttime = time.time()
                            try:
                                req = urllib.request.Request(FURL, headers=hdr)
                                with urllib.request.urlopen(req) as response:
                                        rankedstats = json.loads(response.read().decode("UTF-8"))
                                        
                                read = True
                                
                            except urllib.error.HTTPError as err:
                                if str(err).rstrip() != "HTTP Error 429: Too Many Requests":
                                    invName = True
                                    break
                                
                                time.sleep(1.5)
                    #Here we have the ranked data, and we're going to populate the fields that we
                    #Want. They are Last Updated, Status, and then for each champion we have total
                    #games played, wins, losses, average kills, average deaths, and average assists.
                    datadict[dat]['LstUpd'] = time.time()
                    if invName:
                        datadict[dat]['Status'] = "EMPTY"
                    else:
                        datadict[dat]['Status'] = "OK"

                        champs = rankedstats['champions']
                        for ch in champs:
                            datadict[dat]['stats'][ch['id']] = {}
                            datadict[dat]['stats'][ch['id']]['SoloGames'] = ch['stats']['totalSessionsPlayed']
                            datadict[dat]['stats'][ch['id']]['Ws'] = ch['stats']['totalSessionsWon']
                            datadict[dat]['stats'][ch['id']]['Ls'] = ch['stats']['totalSessionsLost']
                            datadict[dat]['stats'][ch['id']]['AvKills'] = float(ch['stats']['totalChampionKills'] / ch['stats']['totalSessionsPlayed'])
                            datadict[dat]['stats'][ch['id']]['AvDeaths'] = float(ch['stats']['totalDeathsPerSession'] / ch['stats']['totalSessionsPlayed'])
                            datadict[dat]['stats'][ch['id']]['AvAssists'] = float(ch['stats']['totalAssists'] / ch['stats']['totalSessionsPlayed'])

                #Atleast one name unknown
                array.append(datadict)
                response = bytes("@" + json.dumps(datadict) + "@", 'ascii')
                self.request.sendall(response)
                
            else: #This indicates an error
                response = bytes("@ERROR@", 'ascii')
                self.request.sendall(response)
        else: #This is where all of the names were already known
            response = bytes("@" + json.dumps(datadict) + "@", 'ascii')
            self.request.sendall(response)

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


if __name__ == "__main__":

    #Establish port and host for use in server.
    HOST, PORT = "localhost", 22222

    server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler)
    ip, port = server.server_address

    # Start a thread with the server -- that thread will then start one
    # more thread for each request
    server_thread = threading.Thread(target=server.serve_forever)
    # Exit the server thread when the main thread terminates
    server_thread.daemon = True
    server_thread.start()
    print("Server loop running in thread:", server_thread.name)


    #This acts as the main "thread". Really this code is running always and
    #can freely interact with global variables. It is used as a threadsafe
    #Way to update the SQL database and validdict / validnames elements.
    while(1):
        try:
            if len(array) > 0:
                for key in array[0]: #These should be the names searched with a dictionary
                    txt = json.dumps(array[0][key])
                    curs.execute("INSERT OR REPLACE INTO RANKED(id, info) VALUES(?,?)", (key, txt))
                    conn.commit()
                    if key not in validnames:
                        validnames.append(key)
                    validdict[key] = array[0][key]
                array.pop(0)
        except KeyboardInterrupt:
            print("Shutting down and exiting. Goodbye!")
            #Close connection to SQL database.
            conn.close()
            break
        
    #Last step before exitting is to kill the server
    server.shutdown()
