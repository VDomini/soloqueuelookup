import tkinter.ttk
from tkinter.constants import *
import socket
import errno
import logging
import json
import urllib.request
from tkinter import messagebox



#This class constructs a tkinter application which will be the
#client for this program.
class Application(tkinter.ttk.Frame):

    @classmethod
    #This acts as the main method of the class when it is called.
    #The window is created, root weights are given for columns, and
    #the mainloop is called.
    def main(cls):
        tkinter.NoDefaultRoot()
        root = tkinter.Tk()
        app = cls(root)
        app.grid(sticky=NSEW)
        root.grid_columnconfigure(0, weight=1)
        root.grid_columnconfigure(1, weight=1)
        root.grid_columnconfigure(2, weight=1)
        root.grid_columnconfigure(3, weight=1)
        root.resizable(False, True)
        root.mainloop()

    #When an object is created. This function is called. It tells the program to
    #call create_variables (below), create_widgets (below), grid_widgets (below)
    #And finally sets the weight for each column. This illustrates the order in which
    #Functions are called.
    def __init__(self, root):
        super().__init__(root)
        self.create_variables()
        self.create_widgets()
        self.grid_widgets()
        self.grid(sticky=NSEW)
        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure(2, weight=1)
        self.grid_columnconfigure(3, weight=1)

    #The variables to be used in conjunction with each widget are found here
    def create_variables(self):
        #Static display text for entry boxes
        self.player1 = tkinter.StringVar(self, 'Summoner 1')
        self.player2 = tkinter.StringVar(self, 'Summoner 2')
        self.player3 = tkinter.StringVar(self, 'Summoner 3')
        self.player4 = tkinter.StringVar(self, 'Summoner 4')

        #Contents of entry boxes
        self.name1 = tkinter.StringVar(self)
        self.name2 = tkinter.StringVar(self)
        self.name3 = tkinter.StringVar(self)
        self.name4 = tkinter.StringVar(self)

        #These styles change the buttons when they are selected.
        self.sb = tkinter.ttk.Style(self)
        self.sb.configure('blue.TButton', background='blue', foreground = 'blue')
        self.sg = tkinter.ttk.Style(self)
        self.sg.configure('green.TButton', background='green', foreground = 'green')

        #This will contain return data from the server
        self.data = {}

        #This is a static element which is needed to translate numbers from the API
        #To names that the player will be familar with.
        self.champIDs = {0: 'Overall', 1: 'Annie', 2: 'Olaf', 3: 'Galio', 4: '"Twisted Fate"', 5: '"Xin Zhao"',
                         6: 'Urgot', 7: 'Leblanc', 8: 'Vladimir', 9: 'Fiddlesticks', 10: 'Kayle',
                         11: '"Master Yi"', 12: 'Alistar', 13: 'Ryze', 14: 'Sion', 15: 'Sivir',
                         16: 'Soraka', 17: 'Teemo', 18: 'Tristana', 19: 'Warwick', 20: 'Nunu',
                         21: '"Miss Fortune"', 22: 'Ashe', 23: 'Tryndamere', 24: 'Jax', 25: 'Morgana',
                         26: 'Zilean', 27: 'Singed', 28: 'Evelynn', 29: 'Twitch', 30: 'Karthus',
                         31: 'Chogath', 32: 'Amumu', 33: 'Rammus', 34: 'Anivia', 35: 'Shaco',
                         36: '"Dr Mundo"', 37: 'Sona', 38: 'Kassadin', 39: 'Irelia', 40: 'Janna',
                         41: 'Gangplank', 42: 'Corki', 43: 'Karma', 44: 'Taric', 45: 'Veigar',
                         48: 'Trundle', 50: 'Swain', 51: 'Caitlyn', 53: 'Blitzcrank', 54: 'Malphite',
                         55: 'Katarina', 56: 'Nocturne', 57: 'Maokai', 58: 'Renekton', 59: '"Jarvan IV"',
                         60: 'Elise', 61: 'Orianna', 62: 'Wukong', 63: 'Brand', 64: '"Lee Sin"',
                         67: 'Vayne', 68: 'Rumble', 69: 'Cassiopeia', 72: 'Skarner', 268: 'Azir',
                         74: 'Heimerdinger', 75: 'Nasus', 76: 'Nidalee', 77: 'Udyr', 78: 'Poppy',
                         79: 'Gragas', 80: 'Pantheon', 81: 'Ezreal', 82: 'Mordekaiser', 83: 'Yorick',
                         84: 'Akali', 85: 'Kennen', 86: 'Garen', 267: 'Nami', 89: 'Leona', 90: 'Malzahar',
                         91: 'Talon', 92: 'Riven', 96: 'KogMaw', 98: 'Shen', 99: 'Lux', 101: 'Xerath',
                         102: 'Shyvana', 103: 'Ahri', 104: 'Graves', 105: 'Fizz', 106: 'Volibear',
                         107: 'Rengar', 110: 'Varus', 111: 'Nautilus', 112: 'Viktor', 266: 'Aatrox',
                         114: 'Fiora', 115: 'Ziggs', 117: 'Lulu', 119: 'Draven', 120: 'Hecarim',
                         121: 'Khazix', 122: 'Darius', 126: 'Jayce', 127: 'Lissandra', 131: 'Diana',
                         133: 'Quinn', 134: 'Syndra', 143: 'Zyra', 150: 'Gnar', 154: 'Zac', 412: 'Thresh',
                         157: 'Yasuo', 161: 'Velkoz', 420: 'Illaoi', 421: 'RekSai', 113: 'Sejuani',
                         429: 'Kalista', 432: 'Bard', 201: 'Braum', 203: 'Kindred', 222: 'Jinx',
                         223: '"Tahm Kench"', 236: 'Lucian', 238: 'Zed', 245: 'Ekko', 254: 'Vi',
                         164: 'Camille', 202: 'Jhin', 163: 'Taliyah', 240: 'Kled', 427: 'Ivern', 498: 'Xayah',
                         497: 'Rakan'}


    #Each individual widget is constructed here
    def create_widgets(self):

        #These are the Entry boxes, where the user will enter the names
        self.nam1 = tkinter.ttk.Entry(self, textvariable=self.name1)
        self.nam2 = tkinter.ttk.Entry(self, textvariable=self.name2)
        self.nam3 = tkinter.ttk.Entry(self, textvariable=self.name3)
        self.nam4 = tkinter.ttk.Entry(self, textvariable=self.name4)

        #this is the lookup button which will call the lookup command when clicked.
        self.lookup = tkinter.ttk.Button(self, text='Lookup Summoners', command=self.lookup)

        #These are the focus buttons, which will display only a specific player's stats at a time
        self.focus1 = tkinter.ttk.Button(self, text='', command=lambda: self.focus(1))
        self.focus2 = tkinter.ttk.Button(self, text='', command=lambda: self.focus(2))
        self.focus3 = tkinter.ttk.Button(self, text='', command=lambda: self.focus(3))
        self.focus4 = tkinter.ttk.Button(self, text='', command=lambda: self.focus(4))

        #These are the static labels above the Entry boxes.
        self.display1 = tkinter.ttk.Label(self, textvariable=self.player1)
        self.display2 = tkinter.ttk.Label(self, textvariable=self.player2)
        self.display3 = tkinter.ttk.Label(self, textvariable=self.player3)
        self.display4 = tkinter.ttk.Label(self, textvariable=self.player4)

        #This establishes the treeview, which is the multi-column listbox seen in the program
        self.lb = tkinter.ttk.Treeview(self, columns= ('Champion', 'Games Played', 'W/L', 'Average K/D/A') )

        #This adds scrolling to the treeview
        self.ysb = tkinter.ttk.Scrollbar(self, orient='vertical', command=self.lb.yview)
        self.xsb = tkinter.ttk.Scrollbar(self, orient='horizontal', command=self.lb.xview)
        self.lb.configure(yscroll=self.ysb.set, xscroll=self.xsb.set)

        #This removes the blank first column of the treeview
        self.lb['show'] = 'headings'

        #This allows the column names to be displayed.
        self.lb.heading("Champion", text="Champion")
        self.lb.heading("Games Played", text="Games Played")
        self.lb.heading("W/L", text="W/L")
        self.lb.heading("Average K/D/A", text = "Average K/D/A")

        #This centers each entry on the treeview.
        self.lb.column("Champion", anchor="c")
        self.lb.column("Games Played", anchor="c")
        self.lb.column("W/L", anchor="c")
        self.lb.column("Average K/D/A", anchor="c")

        
    #This function defines where the widgets will be placed into the gridview.
    def grid_widgets(self):
        #This is the preset options for each widget. sticky=NSEW makes the widget
        #expand when dragged, and padx and pady give a little space around each widget
        options = dict(sticky=NSEW, padx=5, pady=4)

        #The focus buttons will be on the 4th row
        self.focus1.grid(column=0, row=3, **options)
        self.focus2.grid(column=1, row=3, **options)
        self.focus3.grid(column=2, row=3, **options)
        self.focus4.grid(column=3, row=3, **options)

        #The entry boxes are on the second row
        self.nam1.grid(column=0, row=1, **options)
        self.nam2.grid(column=1, row=1, **options)
        self.nam3.grid(column=2, row=1, **options)
        self.nam4.grid(column=3, row=1, **options)

        #The lookup button is in the third row
        self.lookup.grid(column=0, columnspan = 4, row=2, **options)

        #The static display elements are on the first row.
        self.display1.grid(column=0, row=0, **options)
        self.display2.grid(column=1, row=0, **options)
        self.display3.grid(column=2, row=0, **options)
        self.display4.grid(column=3, row=0, **options)

        #The treeview is on the fifth and bottommost row
        self.lb.grid(column=0, columnspan=4, row =4, **options)
        
        
    #This program contacts the server with the contents of the entry boxes
    def lookup(self):
        #This is for any messageboxes
        options = dict(parent = self)


        #Specify the IP and port of server. In this case, the server is this computer
        #So localhost is used.
        myServerName = 'localhost'
        myServerPort = 22222

        # Create the client side socket
        try:
            myClientSocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        except OSError as err:
            print( "Cannot create client side socket:", err )
            exit( 1 )

        #Create a connection with the server
        connectIsSuccessful = False
        myClientSocket.setblocking( False )
        while not connectIsSuccessful:
            try:
                myClientSocket.connect( ( myServerName, myServerPort ) )
                connectIsSuccessful = True
            except OSError as err:
                if ( err.errno == errno.EISCONN ):
                    connectIsSuccessful = True
            except:
                print( "Something awful happened!" )
                exit( 1 )
        myClientSocket.setblocking( True )
        #Connection is successful, now send something and wait for result.
        try:
            #Put all of the entry box elements into one comma separated list
            myMsg = self.name1.get().rstrip() + "-" + self.name2.get().rstrip() + "-" + self.name3.get().rstrip() + "-" + self.name4.get().rstrip()
            myMsg = myMsg.rstrip().replace("-", ",").rstrip(",")

            #If the entry boxes arent all empty.
            if myMsg != '':
                    
                #Send them to the server
                try:
                    myClientSocket.sendall( (str( myMsg )).encode( 'ascii' ) )
                except OSError as err:
                    print( err, "Something went wrong with sendall function." )
                    myClientSocket.close()

                myClientSocket.settimeout( 0 )
                while True:

                    # The following socket calls are blocking
                    
                    #Wait to receive information from the server
                    try:
                        recvInProgress = True
                        recvFin = False 
                        rcvdStr = ''
                        while recvInProgress:
                            #We're only receiving 1024 bits at a time. So we decode what we
                            #Get and concatenate it to what we already have
                            rcvdStr = rcvdStr + myClientSocket.recv( 1024 ).decode( 'ascii' )
                            if len(rcvdStr) == 0:
                                recvInProgress = False

                            #This is how we know it is the end. The server will ALWAYS begin
                            #and end the message with @. This has been set.
                            if len(rcvdStr) > 0 and ( rcvdStr[ 0 ] == '@' ) and ( rcvdStr[ -1 ] == '@' ):
                                recvInProgress = False
                                recvFin = True
                        #Here we are done receiving and have the full message
                        if recvFin:
                            incMsg = rcvdStr.strip( '@' )
                            #The message could be an error if all of the names were invalid.
                            if incMsg != "ERROR":
                                #If it is not an error, populate the focus boxes and call focus
                                #on the first one.
                                self.data = json.loads(incMsg)
                                self.focus1["text"] = self.name1.get().lower()
                                self.focus2["text"] = self.name2.get().lower()
                                self.focus3["text"] = self.name3.get().lower()
                                self.focus4["text"] = self.name4.get().lower()
                                self.focus(1)
                            else:
                                #If there is an error, display a messagebox informing the user
                                messagebox.showwarning("Error", "Something went wrong retrieving information or there were no valid names given.", **options)
                            break
                    except TimeoutError as err:
                        logging.debug("Caught a timeout")
                    except OSError:
                        pass
                    
                    myClientSocket.settimeout( None )
            else:
                messagebox.showwarning("No Names Provided", "All of the Summoner Name boxes are empty", **options)
        except:
            pass
        


    #This is the focus function. It will be called first by the lookup command and then
    #By any of the focus buttons. Depending on the button pressed, it will load relevant
    #Contents into the treeview to be displayed. Additionally if the name is invalid, it
    #Will tell the user. 
    def focus(self, num):
        nam = ''

        #Each button is called with a different number. This lets us know what button
        #is pressed. Here we see if the button string has been changed (if it has not then
        #no name was called from it). If it has, we make that button green to indicate it has been
        #focused and we make all of the others blue.
        if num == 1:
            if self.focus1["text"] == '':
                return
            else:
                nam = self.focus1["text"]
                self.focus1.configure(style = 'green.TButton')
                self.focus2.configure(style = 'blue.TButton')
                self.focus3.configure(style = 'blue.TButton')
                self.focus4.configure(style = 'blue.TButton')
        if num == 2:
            if self.focus2["text"] == '':
                return
            else:
                nam = self.focus2["text"]
                self.focus2.configure(style = 'green.TButton')
                self.focus1.configure(style = 'blue.TButton')
                self.focus3.configure(style = 'blue.TButton')
                self.focus4.configure(style = 'blue.TButton')
        if num == 3:
            if self.focus3["text"] == '':
                return
            else:
                nam = self.focus3["text"]
                self.focus3.configure(style = 'green.TButton')
                self.focus2.configure(style = 'blue.TButton')
                self.focus1.configure(style = 'blue.TButton')
                self.focus4.configure(style = 'blue.TButton')
        if num == 4:
            if self.focus4["text"] == '':
                return
            else:
                nam = self.focus4["text"]
                self.focus4.configure(style = 'green.TButton')
                self.focus2.configure(style = 'blue.TButton')
                self.focus3.configure(style = 'blue.TButton')
                self.focus1.configure(style = 'blue.TButton')

        #Empty the current treeview
        self.lb.delete(*self.lb.get_children())

        #Creating a copy of champIDs (from create variables above) to reference.
        chIDs = self.champIDs

        if nam in self.data:
            if( self.data[nam]['Status'] == "OK"):
                stats = self.data[nam]['stats']
                #Here we are sorting the dictionary from most played to least played
                #This allows for id 0 (the combined stats) to always be first.
                stats2 = sorted(stats, key=lambda x: stats[x]['SoloGames'], reverse=True)
                
                #For each playable character, display stats. ID of 0 indicates combined stats.
                for champ in stats2:

                    kda = '{:.1f}'.format((stats[champ]['AvKills'])) + '/' + '{:.1f}'.format((stats[champ]['AvDeaths'])) + '/' + '{:.1f}'.format((stats[champ]['AvAssists']))
                    wl = str(stats[champ]['Ws']) + '/' + str(stats[champ]['Ls'])
                    
                    vals = chIDs[int(champ)] + ' ' + str(stats[champ]['SoloGames']) + ' ' + wl + ' ' + kda
                    self.lb.insert('', 'end', text='', values=(vals))
            else: #If the name is present but the element is empty then they have not played ranked
                self.lb.insert('', 'end', text='', values=('"No Info" "No Info" "No Info" "No Info"'))
        else: #If the name is not present, the name was not valid
             self.lb.insert('', 'end', text='', values=('"Invalid Name" "Invalid Name" "Invalid Name" "Invalid Name"'))   
        
#Call the main function of the program
if __name__ == '__main__':
    Application.main()
