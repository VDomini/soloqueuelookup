This repository contains code for both the server and client sides of an application which aims to allow you to look up opponents in the popular MOBA League of Legends in Real Time.

Both the Client and the Server are written in python.